/**
 *
 * RenderPipeline
 *
 * Copyright (c) 2017 Emmanuel Durand <emmanueldurand@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#pragma once

// Convert texcoords to cubemap texcoords
vec2 convert_texcoord_to_cubemap(vec2 tcoord) {
    return mod(tcoord, vec2(1.0 / 3.0, 1.0 / 2.0)) * vec2(3.0, 2.0);
}

// Get the camera ID from the texcoords inside a cubemap
int get_cameraid_from_texcoord(vec2 tcoord) {
    vec2 xycoord = floor(tcoord.xy * vec2(3.0, 2.0));
    float id = xycoord.x + 3.0 * xycoord.y;
    return int(id);
}

// Get the delta rotation matrix for a cubemap camera, based on its index
mat4 get_cubemap_rotation_for_camera(int index) {
#if IM_INWARD
    const mat4 cubemap_camera_rotation[6] = {
        mat4(1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0), // top
        mat4(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0), // front
        mat4(1.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0), // bottom
        mat4(0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, -0.0, 0.0, 0.0, 1.0), // right
        mat4(-1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0, 1.0), // back
        mat4(0.0, 0.0, -1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0) // left
    };
#else
    const mat4 cubemap_camera_rotation[6] = {
        mat4(1.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0), // bottom
        mat4(-1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0, 1.0), // back
        mat4(1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0), // top
        mat4(0.0, 0.0, -1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0), // left
        mat4(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0), // front
        mat4(0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, -0.0, 0.0, 0.0, 1.0) // right
    };
#endif

    return cubemap_camera_rotation[index];
}

// Get the delta rotation matrix for a cubemap camera, based on the texcoord
mat4 get_cubemap_rotation_for_camera(vec2 tcoord) {
    // TODO: the first implementation seems slower, maybe due to the int cast
#if 0
    float view_index = floor(tcoord.x * 3.0) + 3 * floor(tcoord.y * 2.0);
    return get_cubemap_rotation_for_camera(int(view_index));
#else
    if (tcoord.y < 0.5) {
        // Bottom
        if (tcoord.x < 1.0/3.0) {
            return get_cubemap_rotation_for_camera(0);
        }
        // Back
        else if (tcoord.x < 2.0/3.0) {
            return get_cubemap_rotation_for_camera(1);
        }
        // Top
        else {
            return get_cubemap_rotation_for_camera(2);
        }
    }
    else {
        // Left
        if (tcoord.x < 1.0/3.0) {
            return get_cubemap_rotation_for_camera(3);
        }
        // Front
        else if (tcoord.x < 2.0/3.0) {
            return get_cubemap_rotation_for_camera(4);
        }
        // Right
        else {
            return get_cubemap_rotation_for_camera(5);
        }
    }
#endif
}

// Get the cubemap camera offset the view coordinates should be projected onto
mat4 get_cubemap_view_offset(vec3 view_pos, out vec2 screen_offset) {
    // Check which subview the view_pos will be projected onto
    vec3 v = normalize(view_pos);
    float angle_from_front = dot(normalize(vec3(v.x, 0.0, v.z)), vec3(0.0, 0.0, 1.0));
    float angle_from_left = dot(normalize(vec3(v.x, 0.0, v.z)), vec3(1.0, 0.0, 0.0));
    float angle_from_top = dot(normalize(vec3(0.0, v.y, v.z)), vec3(0.0, 1.0, 0.0));

    mat4 cubemap_rotation_mat;
    vec2 cubemap_proj_shift;
    // Front
    if (angle_from_front > HALF_SQRT_TWO) {
        cubemap_rotation_mat = get_cubemap_rotation_for_camera(4);
        cubemap_proj_shift = vec2(1.0 / 3.0, 0.5);
    }
    // Back
    else if (angle_from_front < -HALF_SQRT_TWO) {
        cubemap_rotation_mat = get_cubemap_rotation_for_camera(1);
        cubemap_proj_shift = vec2(1.0 / 3.0, 0.0);
    }
    // Left
    else if (angle_from_left > HALF_SQRT_TWO) {
        cubemap_rotation_mat = get_cubemap_rotation_for_camera(3);
        cubemap_proj_shift = vec2(0.0, 0.5);
    }
    // Right
    else if (angle_from_left < -HALF_SQRT_TWO) {
        cubemap_rotation_mat = get_cubemap_rotation_for_camera(5);
        cubemap_proj_shift = vec2(2.0 / 3.0, 0.5);
    }
    // Top
    else if (angle_from_top > HALF_SQRT_TWO) {
        cubemap_rotation_mat = get_cubemap_rotation_for_camera(2);
        cubemap_proj_shift = vec2(2.0 / 3.0, 0.0);
    }
    // Bottom
    else {
        cubemap_rotation_mat = get_cubemap_rotation_for_camera(0);
        cubemap_proj_shift = vec2(0.0, 0.0);
    }

    screen_offset = cubemap_proj_shift;
    return cubemap_rotation_mat;
}

CONTRIBUTING
============

This document is meant to give the keys to understand how the RenderPipeline is built, especially regarding how the various stages are connected and ordered together. It is not (yet) intended to give any coding style insight, for this you should check the code by yourself.


Pipeline startup
----------------
The startup of the pipeline loads the configuration, the plugins, and initializes all the stages of the renderer. The important steps are:

* the whole stuff is loaded from the RenderPipeline.create method
* RenderPipeline._init_showbase initializes Panda3D and loads the configuration
* RenderPipeline._init_globals intializes some global variables, including the rendering resolution (which can be different from the display resolution, depending on the options of the pipeline)
* RenderPipeline._adjust_camera_settings sets the field of view
* RenderPipeline._create_managers and RenderPipeline._initialize_managers create the various managers and initializes them, including the __tag state manager__ and the __stage manager__, which are of utmost importance for the rendering process


Tag state manager
-----------------

The role of the tag state manager is to manage the various RenderStates assigned to various stages in the pipeline. Scene objects are not necessarily rendered at each stage: for example, transparent objects are rendered in the forward stage, and skipped during the gbuffer pass. This is done through camera masks, with a default camera mask being set at the creation of the tag state manager on the main camera.


Stage manager
-------------

# Stage description

A stage has the following inputs and outputs:

* Inputs:
** RenderStage.required_inputs: inputs the stage needs as uniforms, taken from StageManager.inputs
** RenderStage.required_pipes: a pipe is basically a texture (or a structure containing multiple textures, like a GBufferData) which is rendered to by a stage. This lists the textures needed by the stage, they are sent to the GLSL shaders as uniforms.
* Outputs:
** RenderStage.produced_inputs: method returning a list of variables to be used as inputs for other stages
** RenderStage.produced_pipes: method returning a list of the pipes (hence textures) filled by the stage. These pipes are all visible in the debug view of the RenderPipeline.
** RenderStage.produced_defines: method returning a list of #define to be set in all the GLSL shaders of the pipeline. This allows for setting options which impact the whole pipeline from a single place.

Each stage can have its own pipeline internally, with multiple renders to texture.

# Mandatory stages

Before any plugin being loaded, a few stages are created by default:

* AmbientStage: renders the ambient lighting.
* GBufferStage: renders the geometry buffers, used subsequently in the whole deferred rendering stages.
* FinalStage: prepares the shaded render to be sent to the front buffer, basically downscaling it from 16bits to 8bits per component.
* DownscaleZStage: the depth buffer rendered by the GBufferStage is not stored as a linear value. This stage fixes this, making it easier to use for the next stages.
* CombineVelocityStage: combine the object velocity, computed in the GBufferStage, with the camera velocity
* ImmersiveStage: does nothing, except setting useful options in the GLSL shaders to enable cubemap rendering.

# Plugin stages

Once the plugins are loaded, they are created by StageManager.setup accordingly to the configuration. This method:

* orders the stages accordingly to the file _stages.yaml_
* creates the stages
* gets their produced output (defines, pipes, variables)
* bind their inputs to previously produced outputs

The stage manager also keeps in memory the result of the previous frame, which can be used for rendering effects.

During runtime, the rendering order is handled internally by Panda3D depending on the need of each shader.
